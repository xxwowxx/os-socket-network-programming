# getaddrinfo/freeaddrinfo/gai_strerror

- [man pages](#man-pages)
- [函数签名](#函数签名)
- [函数参数](#函数参数)
- [返回值](#返回值)
- [使用范例](#使用范例)

## man pages

```bash
man getaddrinfo.3
man getaddrinfo.3p

man freeaddrinfo.3
man freeaddrinfo.3p

man gai_strerror.3
man gai_strerror.3p
```

## 函数签名

```c
#include <netdb.h>
#include <sys/socket.h>
#include <sys/types.h>

struct addrinfo {
    int              ai_flags;
    int              ai_family;
    int              ai_socktype;
    int              ai_protocol;
    socklen_t        ai_addrlen;
    struct sockaddr *ai_addr;
    char            *ai_canonname;
    struct addrinfo *ai_next;
};

int getaddrinfo(const char *restrict node,
                const char *restrict service,
                const struct addrinfo *restrict hints,
                struct addrinfo **restrict res);

void freeaddrinfo(struct addrinfo *res);

const char *gai_strerror(int errcode);
```

## 函数参数

```c
// node
// 可以是域名、IP 地址、NULL

// service
// 可以是服务名、端口号

// hints.ai_flags
// 说明用于 bind 函数 - AI_PASSIVE
// 只能使用 IP 地址 - AI_NUMERICHOST
// 只能使用数字端口号 - AI_NUMERICSERV
#include <netdb.h>

// hints.ai_family
// 未指定 IP 版本 - AF_UNSPEC
// ipv4 - AF_INET
// ipv6 - AF_INET6
#include <sys/socket.h>

// hints.ai_socktype
// tcp - SOCK_STREAM
// udp - SOCK_DGRAM
#include <sys/socket.h>

// hints.ai_protocol
// tcp - IPPROTO_TCP
// udp - IPPROTO_UDP
#include <netinet/in.h>

// res
// getaddrinfo 分配堆空间并绑定到 res 指针，使用 freeaddrinfo 释放分配的堆空间

// errcode
// 当 getaddrinfo 出错时会返回错误值，可以使用 gai_strerror 获取对应的错误信息字符串
```

## 返回值

```c
// getaddrinfo
// 正常 - 返回 0 值
// 错误 - 返回非零值错误码 (不会设置 errno 变量)

// freeaddrinfo
// 不会返回值

// gai_strerror
// 返回错误码对应的错误信息字符串 (虽然返回了指针，但是似乎不需要多余的释放行为)
```

## 使用范例

```c
#include <stdio.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

int main(void)
{
	const char *host = "127.0.0.1";
	const char *port = "26214";
	struct addrinfo hints;
	struct addrinfo *res, *p;

	memset(&hints, 0, sizeof(hints));
	hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST | AI_NUMERICSERV;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	int errnv = getaddrinfo(host, port, &hints, &res);
	if (errnv != 0) {
		fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(errnv));
		freeaddrinfo(res);
		return 1;
	}

	for (p = res; p != NULL; p = p->ai_next) {
		printf("addrinfo: %p\n", p);
	}

	freeaddrinfo(res);

	return 0;
}
```
