# listen

- [man pages](#man-pages)
- [函数签名](#函数签名)
- [函数参数](#函数参数)
- [返回值](#返回值)
- [使用范例](#使用范例)

## man pages

```bash
man listen.2
man listen.3p
```

## 函数签名

```c
#include <sys/socket.h>

int listen(int sockfd, int backlog);
```

## 函数参数

```c
// sockfd
// 非负整数 - 有效的 socket 文件描述符 (适用于 tcp 协议，但是 udp 协议不适用)

// backlog
// 非负整数 - 对于 tcp 而言，该值表示等待 accept 的已建立的 socket 队列长度
// SOMAXCONN - 当前 Linux 系统的默认最大 backlog 值 (在编译时就确定)
```

## 返回值

```c
// 正常 - 返回 0 值

// 错误 - 返回 -1 值 (同时设置 errno 变量)
```

## 使用范例

```c
#include <stdio.h>
#include <string.h>

#include <netdb.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>

int main(void)
{
	const char *host = "127.0.0.1";
	const char *port = "26214";
	struct addrinfo hints;
	struct addrinfo *res;
	int errnv;

	memset(&hints, 0, sizeof(hints));
	hints.ai_flags = AI_PASSIVE | AI_NUMERICHOST | AI_NUMERICSERV;
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;
	hints.ai_protocol = IPPROTO_TCP;

	errnv = getaddrinfo(host, port, &hints, &res);
	if (errnv != 0) {
		fprintf(stderr, "getaddrinfo(): %s\n", gai_strerror(errnv));
		freeaddrinfo(res);
		return 1;
	}

	int sockfd = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
	if (sockfd < 0) {
		perror("socket()");
	} else {
		printf("socket() sockfd: %d\n", sockfd);
	}

	errnv = bind(sockfd, res->ai_addr, res->ai_addrlen);
	if (errnv != 0) {
		perror("bind()");
	} else {
		printf("bind() sockfd: %d\n", sockfd);
	}

	freeaddrinfo(res);

	errnv = listen(sockfd, SOMAXCONN);
	if (errnv != 0) {
		perror("listen()");
	} else {
		printf("listen() sockfd: %d\n", sockfd);
	}

	return 0;
}
```
