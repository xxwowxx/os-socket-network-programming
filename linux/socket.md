# socket

- [man pages](#man-pages)
- [函数签名](#函数签名)
- [函数参数](#函数参数)
- [返回值](#返回值)
- [使用范例](#使用范例)

## man pages

```bash
man socket.2
man socket.3p
man socket.7
```

## 函数签名

```c
#include <sys/socket.h>

int socket(int domain, int type, int protocol);
```

## 函数参数

```c
// domain
// 未指定 IP 版本 - AF_UNSPEC
// ipv4 - AF_INET
// ipv6 - AF_INET6
#include <sys/socket.h>

// type
// tcp - SOCK_STREAM
// udp - SOCK_DGRAM
#include <sys/socket.h>

// protocol
// tcp - IPPROTO_TCP
// udp - IPPROTO_UDP
#include <netinet/in.h>
```

## 返回值

```c
// 正常 - 返回非负整数 (表示 socket 的文件描述符)

// 错误 - 返回 -1 值 (同时设置 errno 变量)
```

## 使用范例

```c
#include <stdio.h>

#include <netinet/in.h>
#include <sys/socket.h>

int main(void)
{
	int sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	if (sockfd < 0) {
		perror("socket()");
	} else {
		printf("sockfd: %d\n", sockfd);
	}

	return 0;
}
```
